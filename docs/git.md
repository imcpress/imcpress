# Git

## Setup

IMCPress uses a way to track its code on top of the Wordpress one way similar
to [this one](https://davidwinter.dev/install-and-manage-wordpress-with-git/).

The purpose being to ease development tracking and sharing, customization, as
well as deployment which can be done using CI or simply by pushing to a Git
repo.

IMCPress makes extensive use of the Git submodule feature to track various Git
repos in one project. To get your hands in submodules if you don't know them
yet, [this page](https://git-scm.com/book/en/v2/Git-Tools-Submodules) is a good
start (it can be found in several languages).

This main repo `imcpress.git` only tracks `wp-config.php`, a
`wp-content/` directory, and has the WordPress code inside the `wordpress/`
directory, being a submodule tracking the upstream WordPress Git.

It also brings a `config/` directory, which contains local installation secrets
that won't be tracked by Git (`database.php` and `seeds.php`).

Then in the `wp-content/` directory a few more Git submodule are tracked:

The main code to turn WordPress as an IMC lays in the `imcpress-plugin.git`
submodule, while the  `imcpress-theme.git` submodule holds the frontend
theming.

There's also a couple of default plugins tracked as submodules
('classic-editor' and 'unbloater')

All in all the tree looks like

    / (imcpress.git)
    ├── config/
    ├── wordpress/ (github's WordPress.git svn mirror)
    └── wp-content/
        ├── languages/
        ├── plugins/
        │   ├── classic-editor/ (../classic-editor.git)
        │   ├── imcpress/ (../imcpress-plugin.git)
        │   └── unbloater/ (../unbloater.git)
        └── themes/
            └── imcpress-theme/ (../imcpress-theme.git)

## Usage

Getting the whole code is as easy as cloning this repo and its submodules as in:

`$ git clone --recurse-submodules https://0xcab.org/imcpress/imcpress.git`

For most of the pulling operations, it's necessary to use the
`--recurse-submodules` option.

Then whenever a change is made in a submodule, it needs to be commited in its
git repo, and the change also needs to be commited in the main imcpress.git
repo so that it knows which commit of the submodule it needs to checkout.

Branches and tags are usable as usual in every of this repos.

