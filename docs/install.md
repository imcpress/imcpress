# IMCPress installation

To install your own instance of IMCPress, you need to have a configured
webserver able to run PHP, a SQl database configured, and the Git software.

You can get the IMCPress code by cloning its main repo and its submodules:

`$ git clone --recurse-submodules https://0xacab.org/imcpress/imcpress.git`

Copy the wordpress subdirectory in your vhost root:

`$ cp -a imcpress/wordpress /var/www/something/`

N.B: take care that the wordpress files are in the `wordpress` subdirectory of
your vhost root. Depending on your webserver configuration, you may need to
adjust ownership and permissions.

Then point your browser to this subdirectory of you webserver (e.g
http://localhost/wordpress/) and install WordPress.

Once done, backup the wordpress/wp-config.php file and remove the wordpress/
subdiretory from your vhost root.

Copy the IMCPress code into your vhost root directory, e.g:

`$ rsync -av imcpress/ /var/www/something/`

Don't forget the trailing slashes to the directories so that you don't end up
with the IMCPress code being in a imcpress/ subdirectory of your vhost root.

Then create the config/seeds.php and config/database.php from the provided
sample:

`$ cd /var/www/something/`
`$ cp config/database-sample.php config/database.php`
`$ cp config/seeds-sample.php config/seeds.php`

Fill both config/database.php and config/seeds.php with the values you can find
in the previously backed up wp-config.php.

Login in your installation admin area by pointing your browser to the login page
(e.g http://localhost/wordpress/wp-login.php) and using the credentials you set
up during the installation. Go in the plugin menu and activate all of them, then
in the theme menu and activate the IMCPress theme.

You now have an (empty) IMCPress up and running!

If you need content to populate your pages and start hacking, maybe the
[Fakerpress](https://wordpress.org/plugins/fakerpress/) or [WP Dummy Content
Generator](https://wordpress.org/plugins/wp-dummy-content-generator/) plugins can
help you.

