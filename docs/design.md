# Design

Rather than testing and installing a bunch of plugins, the choice has been to
hack wordpress.

Plugins are more often than not responsible for security breaches and most of
them may not completely fit the purpose of an IMC. Let's not say they also
would require in depth examination regarding their behavior versus contributors
and moderators privacy.

On the other hand WordPress is easily customizable with minimal coding.

To ease development and deployment, IMCPress heavily rely on Git and its
submodule feature. See [the documentation about Git](docs/git.md) for more
informations about that.

Spolier: Getting the whole code is as easy as cloning this repo and
its submodules as in:

`$ git clone --recurse-submodules https://0xcab.org/imcpress/imcpress.git`

