<?php

$publish_page_slug = 'publish';

// Custom post status
$imcpress_statuses = array(
	array(
		'slug'	=> 'feature',
		'args'	=> array(
			'label'		=> 'Feature',
			'public'	=> true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'label_count'               => _n_noop(
				'Feature <span class="count">(%s)</span>',
				'Feature <span class="count">(%s)</span>'
			)
		),
	),
);

// Taxonomies
$imcpress_taxonomies = array(
	array(
		'slug'   => 'place_tag',
		'labels' => array(
			'name' => 'Place tags',
		),
		'args' => array(
			'hierarchical' => false,
		),
	),
	array(
		'slug'   => 'wire',
		'labels' => array(
			'name' => 'Wire',
		),
		'type'   => 'dropdown',
		'args'   => array(
			'hierarchical' => true,
			'default_term' => array(
				array(
					'name'        => 'Local',
					'slug'        => 'local',
					'description' => 'Info locales',
				),
				array(
					'name'        => 'Global',
					'slug'        => 'global',
					'description' => 'Info globales',
				),
			),
		),
	),
);

// Types
$imcpress_post_types['event'] = array(
	'supports'      => array(
		'title',
		'thumbnail',
		'editor',
		'author',
		'custom-fields',
		'comments',
	),
	'menu_position' => 6,
	'menu_icon'     => 'dashicons-calendar',
	'taxonomy_slug' => array( 'post_tag', 'place_tag' ),
	'metaboxes'     => array(
		array(
			'slug'     => 'details',
			'label'    => 'Event details',
			'position' => 'side',
			'metadata' => array(
				array(
					'slug'  => 'date',
					'label' => 'Event date:',
					'type'  => 'datetime',
				),
				array(
					'slug'  => 'place',
					'label' => 'Event place:',
					'type'  => 'textarea',
				),
			),
		),
	),
);

$imcpress_post_types['zine'] = array(
	'supports'      => array(
		'title',
		'editor',
		'author',
		'custom-fields',
		'thumbnail',
		'comments',
	),
	'menu_position' => 8,
	'menu_icon'     => 'dashicons-media-interactive',
	'taxonomy_slug' => array( 'post_tag' ),
);

$imcpress_post_types['tumble'] = array(
	'supports'      => array(
		'editor',
		'author',
		'custom-fields',
		'thumbnail',
	),
	'menu_position' => 7,
	'menu_icon'     => 'dashicons-testimonial',
	'taxonomy_slug' => array( 'post_tag', 'place_tag' ),
);
