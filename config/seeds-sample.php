<?php

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'XXX' );
define( 'SECURE_AUTH_KEY',  'XXX' );
define( 'LOGGED_IN_KEY',    'XXX' );
define( 'NONCE_KEY',        'XXX' );
define( 'AUTH_SALT',        'XXX' );
define( 'SECURE_AUTH_SALT', 'XXX' );
define( 'LOGGED_IN_SALT',   'XXX' );
define( 'NONCE_SALT',       'XXX' );

/**#@-*/
