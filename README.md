# IMCpress

Adapting WordPress to run an Indymedia website.


## Overview

Indymedia websites are using
[open-publishing](https://en.wikipedia.org/wiki/Open_publishing) so that
contributors can openly and easily share news from the streets without the
filtering and tracking faced when using media corporations or social medias.

It implies the ability to publish news from the frontend, without requiring a
login. Publishing from the backend with a login is sometimes also possible.

The contributions are moderated by a collective following an editorial policy,
and the moderation decisions are openly available to anyone.

That's the main features IMCPress brings to WordPress, as well as a few others


## Features

Imcpress ships the following features once installed:

- **Custom contribution statuses** to adapt the moderation flow to an IMC

- **Custom contribution types**, in addition to the default `post` type:
  - **events**, pretty self explanatory
  - **zines**, for booklets
  - **tumbles** for short news

- **Custom taxonomies:**
  - a **wire** tag available to sort WordPress inative post type in different
    exclusive categories, enabling to e.g put them in different sidebars or
    different places on the homepage
  - a **place_tag** tag for all contributions types to sort them by place

- **A frontend form** is automatically generated for each contribution type, so
  that contributors can post their contribution without requiring a login.

- **Adapted commenting system:** they are always possible, but can be either
  visible upon posting, or needing a validation to appear on the frontend. This
  is configurable, and can be set to depend on which wire, status or type of
  their contribution is part of. This can also be configured per contribution.

- **Stripped down contributor role** so that contributors can still use the
  admin area if they wish so to post their contribution. This way they can
  manage drafts before submitting it

- **A custom author field** so that contributors can choose whatever author name
  is displayed on the frontend

- **Users can be configured as a group:** IMCPress admins can define particular
  users as being a group, which enable to highlight their contributions on the
  homepage and display each group contributions on a dedicated page

- **Customizable:** cusom statuses, taxonomies and types can be customized:
  they can be removed, modified, or some new ones can be added at will. The
  automatically generated frontend form will adapt to this customization

- **Users privacy:** care have been taken to protect users. IMCPress supports
  to be reached through a Tor hidden service, as well as obfuscating users IP
  address at the webserver level.

- **Translatable:** the plugin and theme are [translatable](docs/l10n.md)

- **Ships some default Wordpress configuration** so that Wordpress is
  preconfigured to play well with IMCPress, without human intervention

- Ships a few default preconfigured plugins :
  - [unbloater](https://wordpress.org/plugins/unbloater/) to remove some
  unwanted Wordpress 'features'
  - [classic-editor](https://wordpress.org/plugins/classic-editor/) as it's the
  default supported way to edit contributions


## Installing

See [the install documentation](docs/install.md) to install your own.

